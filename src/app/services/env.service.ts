import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvService {

  APP_URL = 'API_URL_HERE';

  constructor() { }
}
